<?php

/**
 * @file
 * Administration page callbacks for the MobDrupal module.
 */

/**
 * Menu callback; displays the MobDrupal administration page.
 */
// function mobdrupal_admin_overview() {
//   return mobdrupal_view();
// }
//
// /**
//  * Displays the MobDrupal administration page.
//  *
//  * @return
//  *   The page HTML.
//  */
// function mobdrupal_view() {
//
//   return mobdrupal_admin_form();
// }
//
// /**
//  * Form builder. Configure MobDrupal
//  *
//  * @ingroup forms
//  */
// function mobdrupal_admin_form($form, $form_state) {
//   // Global aggregator settings.
//   $form['mobdrupal_settings'] = array(
//     '#type' => 'textfield',
//     '#title' => t('Site ID'),
//     '#size' => 80,
//     '#maxlength' => 64,
//     '#default_value' => variable_get('md_site_id', '999999'),
//     '#description' => t('A site id.'),
//   );
//
//
//
//   return $form;
// } // mobdrupal_admin_settings()
